
import {combineReducers} from "redux"
import cartReducer from "./Cart"
export default combineReducers({
    Cart: cartReducer
})