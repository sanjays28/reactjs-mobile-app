import React, { useEffect, useState } from 'react'
import ProductItem from "./../../Share/Components/ProductItem"
import {getProducts} from "./../../Services/Api"
import ProductItemLoading from "./../../Share/Components/Loading/ProductItemLoading"
function Index() {
    const [productNew, setNewProduct] = useState([])
    const [producFeature, setFeatureProduct] = useState([])
    const [loading, setLoading] = useState({
        loadingProductNew: false,
        loadingProductFeature: false
    })

    useEffect(()=>{
        setLoading({...loading, 
            loadingProductNew: true,
            loadingProductFeature: true
        })
        getProducts({params: {limit: 6} }).then(({data})=>{
            console.log(data)
            setLoading({...loading, 
                loadingProductNew: false,
            })
            setNewProduct(data.data.docs)
        })

        getProducts(
            {
                params: {
                    limit: 6,
                    isFeatured: true
            }})
        .then(({data})=>{
            setFeatureProduct(data.data.docs)
            setLoading({...loading, 
                loadingProductFeature: false,
            })

        })
    }, [])
    return (
        <>
            {/*	Feature Product	*/}
            <div className="products">
                <h3>Sản phẩm mới</h3>
                <div className="product-list card-deck">
               
                   { !loading?.loadingProductNew 
                        ? (productNew.map((product)=>{
                           return <ProductItem key={product._id} item={product}/>
                       }))
                       : (<ProductItemLoading/>)
                   }
                   
                   

                </div>
               
            </div>


            {/*	Latest Product	*/}
            <div className="products">
                <h3>Sản phẩm nổi bật</h3>
                <div className="product-list card-deck">
                
                   {
                       !loading?.loadingProductNew
                       ?(producFeature.map((product)=>{
                           return <ProductItem key={product._id} item={product}/>
                       }))
                       :(<ProductItemLoading/>)
                   }
                </div>
               
                
               
            </div>

        </>
    )
}

export default Index
