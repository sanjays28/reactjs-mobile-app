import React from 'react'
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'

function ProductItemLoading( {loop} ) {
    function renderLoading(){
        const arr = new Array(loop).fill(loop)
        return arr
    }
    return (
        <>
            {renderLoading().map((item, index)=>{
                return(

                        <div className="product-item card text-center">
                            <Skeleton  height ="300px"/>
                            <Skeleton  height ="20"/>
                            <Skeleton  height ="20"/>
                        </div>
                 
                )
            })}
            
            
        </>
    )
}
ProductItemLoading.defaultProps = {
    loop: 6
}

export default ProductItemLoading
